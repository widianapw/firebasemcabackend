<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inbox;
use App\Outbox;

class ChatController extends Controller
{
    public function sendChat(Request $request){
        Inbox::insert($request->all());
        $request['message_type'] = "2";
        Outbox::insert($request->all());
        return $request;
    }
}
